window.addEventListener('load', async () => {
	const access = await navigator.requestMIDIAccess();
	const inputs = Array.from(access.inputs.values());
	const outputs = Array.from(access.outputs.values());

	if (inputs.length > 0) {
		document.body.appendChild(document.createTextNode(inputs[0].name));
		inputs[0].addEventListener('midimessage', event => {
			const command = event.data[0] >> 4;
			switch (command) {
				case 8: { // 1000 Note Off event.
					const note = event.data[1];
					document.getElementById(note).remove();
					break;
				}
				case 9: { // 1001 Note On event.
					const note = event.data[1];
					const noteSpan = document.createElement('span');
					noteSpan.textContent = note;
					noteSpan.id = note;
					document.body.appendChild(noteSpan);
					break;
				}
				case 15: { // 1111 System Exclusive. (OP-1 specific?)
					break;
				}
				default: {
					console.log(command, channel, note, velocity, (command >>> 0).toString(2));
				}
			}
		});
	}
});
