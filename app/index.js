window.addEventListener('load', async _ => {
    const fileInput = document.getElementById('fileInput');
    const playPauseButton = document.getElementById('playPauseButton');
    const stopButton = document.getElementById('stopButton');
    const timeInput = document.getElementById('timeInput');
    const renderCanvas = document.getElementById('renderCanvas');

    let channelNotes = [];
    let tempos = [];

    fileInput.addEventListener('change', _ => {
        const file = fileInput.files[0];
        const fileReader = new FileReader();

        fileReader.addEventListener('load', _ => {
            // TODO: Do this in a Web Worker and report progress to the UI.
            // TODO: Rewrite this myself to not have to rely on a 3rd part library
            const data = new SmfParser().parse(fileReader.result);

            // TODO: What is this?
            //console.log('systems', data.systems); // timeSignature & endOfTracks & setTempo's
            //^ same as tracks[0] and tracks[1]

            // Deadmau5 - Strobe from tilt1981 uses 4 tracks
            //console.log('tracks[0]', data.tracks[0]); // timeSignature & endOfTracks
            //console.log('tracks[1]', data.tracks[1]); // setTempo's

            // https://www.youtube.com/watch?v=ppLnKRObtM0
            channelNotes = [ [ ...getNotes(data.tracks[2]) ], [ ...getNotes(data.tracks[3]) ] ];
            tempos = [ ...getTempos(data.tracks[1]) ];

            timeInput.setAttribute('max', Math.max(channelNotes[0][channelNotes[0].length - 1].offTime, channelNotes[1][channelNotes[1].length - 1].offTime));
            render(renderCanvas, channelNotes, tempos, 0);
        });

        fileReader.readAsBinaryString(file);
    });

    // TODO: Replace with RAF
    let playbackInterval;

    playPauseButton.addEventListener('click', _ => {
        switch (playPauseButton.textContent) {
            case '▶': {
                playbackInterval = window.setInterval(() => {
                    timeInput.value = timeInput.valueAsNumber + 1;
                    render(renderCanvas, channelNotes, tempos, timeInput.valueAsNumber);
                }, 1);
                playPauseButton.textContent = '❚❚';
                break;
            }
            case '❚❚': {
                window.clearInterval(playbackInterval);
                playPauseButton.textContent = '▶';
                break;
            }
            default: throw new Error(`Unexpected play pause button text content: ${playPauseButton.textContent}`);
        }
    });

    stopButton.addEventListener('click', _ => {
        window.clearInterval(playbackInterval);
        timeInput.value = '0';
        render(renderCanvas, channelNotes, tempos, 0);
    });

    timeInput.addEventListener('input', _ => {
        window.clearInterval(playbackInterval);
        render(renderCanvas, channelNotes, tempos, timeInput.valueAsNumber);
    });
});

const COLOR_WHITE = 0;
const COLOR_BLACK = 1;

function getNote(number) {
    const octave = Math.floor(number / 12);
    number = number % 12;
    let name;
    let color;
    switch (number) {
        case 0: name = 'C'; color = COLOR_WHITE; break;
        case 1: name = 'C#'; color = COLOR_BLACK; break;
        case 2: name = 'D'; color = COLOR_WHITE; break;
        case 3: name = 'D#'; color = COLOR_BLACK; break;
        case 4: name = 'E'; color = COLOR_WHITE; break;
        case 5: name = 'F'; color = COLOR_WHITE; break;
        case 6: name = 'F#'; color = COLOR_BLACK; break;
        case 7: name = 'G'; color = COLOR_WHITE; break;
        case 8: name = 'G#'; color = COLOR_BLACK; break;
        case 9: name = 'A'; color = COLOR_WHITE; break;
        case 10: name = 'A#'; color = COLOR_BLACK; break;
        case 11: name = 'B'; color = COLOR_WHITE; break;
        default: throw new Error(`Expected number to be within 0 and 11, but it is ${number}.`);
    }

    return { octave, number, name, color };
}

function* getNotes(track) {
    let time = 0;
    let notes = {};
    for (const event of track) {
        time += event.deltaTime;
        switch (event.type) {
            case 'channel': {
                switch (event.subtype) {
                    case 'noteOn': {
                        notes[event.noteNumber] = time;
                        break;
                    }
                    case 'noteOff': {
                        yield ({ ...getNote(event.noteNumber), onTime: notes[event.noteNumber], offTime: time, no: event.noteNumber });
                        delete notes[event.noteNumber];
                        break;
                    }
                    case 'controller': break;
                    case 'pitchBend': break;
                    case 'programChange': break;
                    default: console.log(event);
                }
                
                break;
            }
            case 'meta': {
                switch (event.subtype) {
                    case 'trackName': break;
                    case 'endOfTrack': break;
                    default: console.log(event);
                }

                break;
            }
            default: console.log(event);
        }
    }
}

function* getTempos(track) {
    let time = 0;
    for (const event of track) {
        time += event.deltaTime;
        switch (event.subtype) {
            case 'setTempo': {
                yield ({ time, tempo: event.microsecondsPerBeat });
                break;
            }
            default: console.log(event);
        }
    }
}

const channelColors = [
    undefined,
    [ [ 171, 205, 239 ], [ 0, 128, 255 ] ],
    [ [ 254, 220, 186 ], [ 255, 128, 0 ] ],
];

// https://stackoverflow.com/a/23071105/2715716
// TODO: Reflect tempo changes in time
function render(canvas, channels, tempos, time) {
    const rect = canvas.getBoundingClientRect();
    const width = rect.width;
    let height = rect.height;
    const top = height + time;
    const unit = width / 128;

    // Apply the implicit dimensions so that context dimensions match.
    canvas.width = width;
    canvas.height = height;
    const context = canvas.getContext('2d');
    context.fillRect(0, 0, width, height);

    const keyboardHeight = height / 5; // 20 % of the height
    height -= keyboardHeight;

    let pressedNotes = {};
    let channelNumber = 0;
    for (const notes of channels) {
        channelNumber++;

        // Draw all fill first so that they do not overlap any strokes
        for (const note of notes) {
            const { x, y, w, h } = getNoteXYWH(note, unit, top);
            if (y + h < 0 || y > height) continue;
            let color;
            switch (note.color) {
                case COLOR_WHITE: color = channelColors[channelNumber][0]; break;
                case COLOR_BLACK: color = channelColors[channelNumber][1]; break;
                default: throw new Error(`Invalid color ${note.color}, should be ${COLOR_WHITE} or ${COLOR_BLACK}.`);
            }

            if (y <= height && y + h > height) {
                const ratio = 1 - (((y + h) - height) / h);
                pressedNotes[note.name + note.octave] = `rgba(${color[0]}, ${color[1]}, ${color[2]}, ${ratio})`;
            }

            context.fillStyle = `rgba(${color[0]}, ${color[1]}, ${color[2]}, 1)`;
            context.fillRect(x, y, w, h);
        }

        // Draw all strokes second so they are not overlapped by any fills
        let noteNumber = 0;
        context.strokeStyle = 'rgb(255, 255, 255)';
        context.fillStyle = 'rgb(255, 255, 255)';
        for (const note of notes) {
            noteNumber++;
            const { x, y, w, h } = getNoteXYWH(note, unit, top);
            if (y + h <= 0 || y > height) continue;
            context.strokeRect(x, y, w, h);
            context.fillText(`${note.name}${note.octave}`, x + w, y + h);
        }
    }

    context.fillStyle = 'red';
    context.fillText(time, 0, height);

    // TODO: Calculate and render the "overlap" of white keys into the black keys.
    for (let number = 0; number < 128; number++) {
        const note = getNote(number);
        let noteHeight;
        let noteWidth; // TODO: Get rid of this hack!!
        switch (note.color) {
            case COLOR_WHITE: context.fillStyle = 'white'; context.strokeStyle = 'black'; noteHeight = keyboardHeight; noteWidth = unit * 2; break;
            case COLOR_BLACK: context.fillStyle = 'black'; context.strokeStyle = 'white'; noteHeight = keyboardHeight / 2; noteWidth = unit; break;
            default: throw new Error(`Invalid color ${note.color}, should be ${COLOR_WHITE} or ${COLOR_BLACK}.`);
        }

        context.fillRect(number * unit, height, noteWidth, noteHeight);
        if (pressedNotes[note.name + note.octave]) {
            context.fillStyle = pressedNotes[note.name + note.octave];
            context.fillRect(number * unit, height, noteWidth, noteHeight);
        }

        context.strokeRect(number * unit, height, noteWidth, noteHeight);
    }
}

function getNoteXYWH(note, unit, top) {
    let w;
    switch (note.color) {
        case COLOR_WHITE: w = unit; break;
        case COLOR_BLACK: w = unit / 2; break;
        default: throw new Error(`Invalid color ${note.color}, should be ${COLOR_WHITE} or ${COLOR_BLACK}.`);
    }

    const h = note.offTime - note.onTime;
    const x = note.no * unit;
    const y = top - note.onTime - h;
    return { x, y, w, h };
}
