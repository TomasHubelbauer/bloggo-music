# Music

![OP-1](op-1.svg)

- [Ableton's *Learning Music* online application](https://learningmusic.ableton.com/)
- [OP-1 quick guide](https://www.teenageengineering.com/guides/op-1/quick-guide)
- [OP-1 guide](https://www.teenageengineering.com/guides/op-1)
- [OP-1 firmware hacking](https://www.operator-1.com/index.php?p=/discussion/2232/custom-firmware-on-the-op-1)
  - [OP-1 Hacks on GitHub](https://github.com/op1hacks)
- [Chords and their notes](http://www.pianochord.com/)
- [Midifi](https://github.com/midifi/midifi) - an open source Synthesia alternative

https://news.ycombinator.com/item?id=17817403

https://ianring.com/musictheory/scales/

https://www.hacklily.org/

## VCV

https://vcvrack.com/

## Sonic Pi

https://github.com/samaaron/sonic-pi

https://github.com/samaaron/sonic-pi/issues/1902

## AudioKit

https://github.com/AudioKit/AudioKit

## App idea

[Here](app)

Players for inspiration on how to incorporate tempo changes to timing:

- [ryoyakawai/smfplayer](https://github.com/ryoyakawai/smfplayer)
  - [This in particular](https://github.com/ryoyakawai/smfplayer/blob/master/js/smfPlayer.js#L154)
- [cwilso/SMFPlayer](https://github.com/cwilso/SMFPlayer)
- [gree/smfplayer](https://github.com/gree/smfplayer.js)

- Use [titl1981's MIDI files](http://tilt.000webhostapp.com/)
- Use [`smfplayer`'s SMF parser](https://github.com/ryoyakawai/smfplayer/blob/master/js/smfParser.js) (BSD-3-Clause)
- Do Synthesia-like bars falling onto the musical keyboard timing effect
- Register MIDI input and compare accuracy for hitting each bar, report mishit bars

## Web MIDI

- [W3C Working Draft](https://www.w3.org/TR/webmidi/)
- [W3C Editor's Draft](https://webaudio.github.io/web-midi-api/)
- [Can I use?](https://caniuse.com/#search=midi)
- [My Playground](https://bloggo.herokuapp.com/music/midi/)
- [Making the Web Rock by Chris Wilson](https://webaudiodemos.appspot.com/slides/webmidi.html)
  - [Synthetizer](https://github.com/cwilso/midi-synth)
  - [Drum Machine Demo](https://github.com/cwilso/MIDIDrums)
- [MIDI Specs](https://www.midi.org/specifications)
  - [MIDI Messages](https://www.midi.org/specifications/item/table-1-summary-of-midi-message)
- [MIDI Note Names](https://newt.phys.unsw.edu.au/jw/notes.html)
- [Standard MIDI Player Demo](https://github.com/ryoyakawai/smfplayer)

## Arduino MIDI

- [MIDIUSB](https://www.arduino.cc/en/Reference/MIDIUSB)
- [Midi Device](https://www.arduino.cc/en/Tutorial/MidiDevice)

## Links

### [Let's Learn About Waveforms](http://waveforms.surge.sh/waveforms-intro)

[Source on GitHub](https://github.com/joshwcomeau/waveforms)

### [How Generative Music Works](http://teropa.info/loop/) by [Tero Parviainen](https://twitter.com/teropa)

A beautiful and captivating presentation about generative music.

### [A WebAudio experiment a day in December 2017](http://aphall.com/) by [Andy Hall](http://aphall.com/about/)

### nCoda

https://github.com/ncoda

## WebAudio API Based Online DAWs

- [SoundTrap](https://www.soundtrap.com/studio/)
- [Online Sequencer](https://onlinesequencer.net/)
- [OpenDAW](http://opendaw.azurewebsites.net/)
- [GridSound](https://gridsound.github.io/daw/)
