# Consider split layout

Either vertically:

```text
{notes from midi file}
{keyboard}
{notes from midi device}
```

Or horizontally:

```text
{notes from midi file}      |      {notes from midi device}
{keyboard with tutor hands} | {keyboard with student hands}
```
